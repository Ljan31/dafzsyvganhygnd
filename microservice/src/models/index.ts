//code generate automatic not edit date: 2023-08-27T04:22:49.795Z
import Models, { DataTypes, S } from 'onbbu/models';
import * as T from '../types';

const model: Models<Partial<T.ModelAttributes>> = new Models<Partial<T.ModelAttributes>>(T.name);

model.define({

	attributes: {

		fullname: DataTypes.STRING,

		time: DataTypes.BIGINT,

		nota: DataTypes.BIGINT,

	},
	options: { freezeTableName: true }
});

export const sync = (props: S.SyncOptions) => model.sync(props);

export default model;