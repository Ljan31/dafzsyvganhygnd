//code generate automatic not edit date: 2023-08-27T04:20:11.833Z
//@onbbu-name: evaluaciones
export const name: string = "dafzsyvganhygnd";

export interface ModelAttributes {
	id: number

	fullname: string
	time: number
	nota: number

	createdAt: string
	updatedAt: string
}

export interface Create {
	fullname: string
	time: number
	nota: number
}

export interface Udate {
	where: {
		id: number
	}
	params: {
		fullname?: string
		time?: number
		nota?: number

	}
}

export interface Destroy {
	where: {
		id: number
	}
}

export interface FindAndCount {
	where: {
		nota?: number
	}
	paginate: {
		limit?: number
		offset?: number
	}
}




export const endpoint = [
	'create',
	'udate',
	'destroy',
	'findAndCount',
] as const;

export type Endpoint = typeof endpoint[number]