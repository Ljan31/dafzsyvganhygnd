//code generate automatic not edit date: 2023-08-27T04:22:49.798Z
import Worker from 'onbbu/worker';

import * as Services from '../services';
import * as Validate from '../validate';
import * as Middlewares from '../middlewares';

import { Endpoint, name } from '../types';

const worker: Worker<Endpoint> = new Worker<Endpoint>(name);

worker.use("create", Validate.create, Middlewares.create, Services.create);

worker.use("udate", Validate.udate, Middlewares.udate, Services.udate);

worker.use("destroy", Validate.destroy, Middlewares.destroy, Services.destroy);

worker.use("findAndCount", Validate.findAndCount, Middlewares.findAndCount, Services.findAndCount);

export default worker;