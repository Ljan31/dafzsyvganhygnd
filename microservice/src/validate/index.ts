//code generate automatic not edit date: 2023-08-27T04:22:49.803Z
import Joi from 'onbbu/validate';
import * as T from '../types';

export const create = async (params: T.Create): Promise<T.Create> => {

	const schema = Joi.object({
		fullname: Joi.string().max(255).required(),
		time: Joi.number().min(0).required(),
		nota: Joi.number().min(0).required(),
	})

	await schema.validateAsync(params);

	return params;
};

export const udate = async (params: T.Udate): Promise<T.Udate> => {

	const schema = Joi.object({
		where: Joi.object({
			id: Joi.number().min(0).required(),
		}).required(),
		params: Joi.object({
			fullname: Joi.string().max(255),
			time: Joi.number().min(0),
			nota: Joi.number().min(0),
		}).required(),
	})

	await schema.validateAsync(params);

	return params;
};

export const destroy = async (params: T.Destroy): Promise<T.Destroy> => {

	const schema = Joi.object({
		where: Joi.object({
			id: Joi.number().min(0).required(),
		}).required(),
	})

	await schema.validateAsync(params);

	return params;
};

export const findAndCount = async (params: T.FindAndCount): Promise<T.FindAndCount> => {

	const schema = Joi.object({
		where: Joi.object({
			nota: Joi.number().min(0),
		}).required(),
		paginate: Joi.object({
			limit: Joi.number().min(0),
			offset: Joi.number().min(0),
		}).required(),
	})

	await schema.validateAsync(params);

	return params;
};