//code generate automatic not edit date: 2023-08-27T04:22:49.810Z
import Adapters from './adapters';
import { S } from 'onbbu/models';
import Models from './models';

const start = async (props: S.SyncOptions): Promise<void> => {

	await Models.sync(props);

	await Adapters.run();
};

const stop = async (): Promise<void> => {

	await Adapters.stop();
};

export default { start, stop };