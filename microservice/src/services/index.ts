//code generate automatic not edit date: 2023-08-27T04:22:43.029Z
import model from '../models';
import * as T from '../types';
import { NotFoundError, Paginate, Response } from 'onbbu';

export const create = async (params: T.Create): Response<Partial<T.ModelAttributes>> => {

	const data = await model.create(params);
	
	return { statusCode:"success", data: data };
};

export const udate = async ({ where, params }: T.Udate): Response<Partial<T.ModelAttributes>[]> => {

	const {affected, instances} = await model.update(params, {where})

	if( affected === 0) throw new NotFoundError("not found")

	return { statusCode: "success", data: instances };
};

export const destroy = async ({ where }: T.Destroy): Response<Partial<T.ModelAttributes>[]> => {

	const {affected, instances} = await model.destroy({where})

	if( affected === 0) throw new NotFoundError("not found")

	return { statusCode: "success", data: instances };
};

export const findAndCount = async ({ where, paginate }: T.FindAndCount): Response<Paginate<Partial<T.ModelAttributes>>> => {

	const { data, itemCount, pageCount} = await model.findAndCountAll({where, ...paginate})

	if( data.length) return { statusCode:'success', data: { data:[], itemCount:0, pageCount:0} };

	return { statusCode:'success', data: { data, itemCount, pageCount} };
};