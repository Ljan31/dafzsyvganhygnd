//code generate automatic not edit date: 2023-08-27T04:22:49.813Z
import dotenv from "dotenv";
import Subscribe from "onbbu/subscribe";

import dafzsyvganhygnd from "@api/dafzsyvganhygnd";

dotenv.config();

const subscribe: Subscribe = new Subscribe();

/**Example:
subscribe.use("microservice::method", async (instance: TYPE.ModelAttributes) => {
	//Your function here
});
*/

const start = async (): Promise<void> => subscribe.run();

const stop = async (): Promise<void> => subscribe.stop();

export default { start, stop };