//code generate automatic not edit date: 2023-08-27T04:23:32.759Z
import dotenv from 'dotenv';

import Api from 'onbbu/api';

import * as T from './types';

import { Paginate, Response } from "onbbu";

dotenv.config();

export { T };

export const api: Api<T.Endpoint> = new Api<T.Endpoint>(T.name);

export const create = async (params: T.Create): Response<T.ModelAttributes> => {

	const { statusCode, data, message } = await api.add(params, 'create');

	return { statusCode, data, message };
};

export const udate = async (params: T.Udate): Response<T.ModelAttributes[]> => {

	const { statusCode, data, message } = await api.add(params, 'udate');

	return { statusCode, data, message };
};

export const destroy = async (params: T.Destroy): Response<T.ModelAttributes[]> => {

	const { statusCode, data, message } = await api.add(params, 'destroy');

	return { statusCode, data, message };
};

export const findAndCount = async (params: T.FindAndCount): Response<Paginate<T.ModelAttributes>> => {

	const { statusCode, data, message } = await api.add(params, 'findAndCount');

	return { statusCode, data, message };
};

export default {
	create,
	udate,
	destroy,
	findAndCount,
	T,
};