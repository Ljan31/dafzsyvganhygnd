//code generate automatic not edit date: 2023-08-27T04:23:32.761Z
const { raceFunction } = require("onbbu")
const api = require("./dist")

async function create() {

	const instance = await api.create({})

	console.log(instance)
}

async function udate() {

	const instance = await api.udate({})

	console.log(instance)
}

async function destroy() {

	const instance = await api.destroy({})

	console.log(instance)
}

async function findAndCount() {

	const instance = await api.findAndCount({})

	console.log(instance)
}

async function main() {

	try {

		await raceFunction(create)

		await raceFunction(udate)

		await raceFunction(destroy)

		await raceFunction(findAndCount)

	} catch (error) {
		console.log(error)
	}
}

raceFunction(main, 250)